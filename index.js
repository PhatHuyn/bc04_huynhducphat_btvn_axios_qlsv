const BASE_URL = "https://62db6ce8e56f6d82a7728883.mockapi.io";

function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
    // data:{}
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
getDSSV();

function themSV(){
  nameform = document.getElementById("txtTenSV").value;
  emailform = document.getElementById("txtEmail").value;
  mkform = document.getElementById("txtPass").value;
  toanform = document.getElementById("txtDiemToan").value;
  lyform = document.getElementById("txtDiemLy").value;
  hoaform = document.getElementById("txtDiemHoa").value;

  let newSV = {
    name: nameform,
    email:emailform,
    password: mkform,
    math: toanform,
    physics: lyform,
    chemistry: hoaform,
    id: "17",
  };
  console.log('newSV: ', newSV);
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
        document.getElementById("txtMaSV").value = "";
        document.getElementById("txtTenSV").value = "";
        document.getElementById("txtEmail").value = "";
        document.getElementById("txtPass").value = "";
        document.getElementById("txtDiemToan").value = "";
        document.getElementById("txtDiemLy").value = "";
        document.getElementById("txtDiemHoa").value = "";
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function xoaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function resetSV(){
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
  getDSSV();
}

function suaSinhVien(id){
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res.data);
      const datasv = res.data;
      document.getElementById("txtMaSV").value = id;
      document.getElementById("txtTenSV").value = datasv.name;
      document.getElementById("txtEmail").value = datasv.email;
      document.getElementById("txtPass").value = datasv.password;
      document.getElementById("txtDiemToan").value = datasv.math;
      document.getElementById("txtDiemLy").value = datasv.physics;
      document.getElementById("txtDiemHoa").value = datasv.chemistry;
      tatLoading();
    })
    .catch(function (err) {
      console.log(err);
      tatLoading();
    });
}

function capnhatSV(){
  nameform = document.getElementById("txtTenSV").value;
  emailform = document.getElementById("txtEmail").value;
  mkform = document.getElementById("txtPass").value;
  toanform = document.getElementById("txtDiemToan").value;
  lyform = document.getElementById("txtDiemLy").value;
  hoaform = document.getElementById("txtDiemHoa").value;
  idform = document.getElementById("txtMaSV").value;
  let capnhatSV = {
    name: nameform,
    email:emailform,
    password: mkform,
    math: toanform,
    physics: lyform,
    chemistry: hoaform,
    id: idform,
  };
  console.log('capnhatSV: ', capnhatSV);
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${idform}`,
    method: "PUT",
    data: capnhatSV,
  })
    .then(function (res) {
      tatLoading();
      console.log(1);
      console.log(res);
        document.getElementById("txtMaSV").value = "";
        document.getElementById("txtTenSV").value = "";
        document.getElementById("txtEmail").value = "";
        document.getElementById("txtPass").value = "";
        document.getElementById("txtDiemToan").value = "";
        document.getElementById("txtDiemLy").value = "";
        document.getElementById("txtDiemHoa").value = "";
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
      console.log(2);
      console.log(err);
    });
}